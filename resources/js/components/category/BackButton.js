import React, { Component } from "react";
import {Link} from 'react-router-dom';

export default function BackToCategoryList() {
    return (
        <Link to="/category">
            <button className="btn btn btn-primary my-2 my-sm-0" type="submit">
                Category
            </button>
        </Link>
    );
}
