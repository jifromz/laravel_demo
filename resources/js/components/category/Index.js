import React from 'react';
import {  Link, Route} from 'react-router-dom'; 
import Add from './Add';
import Listing from './Listing';
import Edit from './Edit';


function Category() {
    return (
         <div>
            
                <div>
               
                   
                    <Route exact path="/category" component={Listing} />
                    <Route exact path="/category/add" component={Add} />
                    <Route exact path="/category/edit/:id" component={Edit} />
                </div>
            
         </div>
    );
}

export default Category;

 