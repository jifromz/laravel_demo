import React, { Component } from "react";

export default class WarningAlert extends Component {
    render() {
        return (
            <div className="alert alert-warning" role="alert">
                Please provide data.
            </div>
        );
    }
}
