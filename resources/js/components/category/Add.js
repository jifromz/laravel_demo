import React, { Component } from "react";
import axios from "axios";
import SuccessAlert from "./SuccessAlert";
import ErrorAlert from "./ErrorAlert";
import WarningAlert from "./WarningAlert";
import { Link } from "react-router-dom";
import BackToCategoryList from "./BackButton";

export default class Add extends Component {
    constructor() {
        super();
        this.onchangeCategoryName = this.onchangeCategoryName.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.state = {
            category_name: "",
            alert_message: ""
        };
    }

    onchangeCategoryName(e) {
        this.setState({
            category_name: e.target.value
        });
    }

    onSubmit(e) {
        e.preventDefault();

        if (
            this.state.category_name == null ||
            this.state.category_name == ""
        ) {
            this.setState({
                alert_message: "warning"
            });
            return <WarningAlert />;
        }

        const category = {
            category_name: this.state.category_name
        };

        axios
            .post("http://127.0.0.1:8000/api/category/store", category)
            .then(res => {
                this.setState({
                    alert_message: "success"
                });
            })
            .catch(error => {
                this.setState({
                    alert_message: "error"
                });
            });
    }

    render() {
        return (
            <div>
                <hr />
                {this.state.alert_message == "success" ? (
                    <SuccessAlert message={"Category successfully added."} />
                ) : null}
                {this.state.alert_message == "error" ? <ErrorAlert /> : null}
                {this.state.alert_message == "warning" ? (
                    <WarningAlert />
                ) : null}
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label htmlFor="category_name">Category Name</label>
                        <input
                            type="text"
                            className="form-control"
                            id="category_name"
                            value={this.state.category_name}
                            onChange={this.onchangeCategoryName}
                            placeholder="Enter category"
                        />
                    </div>
                    <button type="submit" className="btn btn-primary">
                        Submit
                    </button>{" "}
                    <BackToCategoryList />
                </form>
            </div>
        );
    }
}
