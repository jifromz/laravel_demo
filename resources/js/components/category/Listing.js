import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import SuccessAlert from "./SuccessAlert";
import ErrorAlert from "./ErrorAlert";
export default class Listing extends Component {
    constructor() {
        super();
        this.state = {
            categories: [],
            alert_message: "",
            category_name: ""
        };
        this.onchangeCategoryName = this.onchangeCategoryName.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    componentDidMount() {
        axios.get("http://127.0.0.1:8000/api/category").then(response => {
            this.setState({
                categories: response.data
            });
        });
    }
    

    onDelete(category_id) {
        axios
            .delete("http://127.0.0.1:8000/api/category/delete/" + category_id)
            .then(response => {
                var categories = this.state.categories;

                for (let index = 0; index < categories.length; index++) {
                    if (categories[index].id == category_id) {
                        categories.splice(index, 1);
                        this.setState({
                            categories: categories
                        });
                    }
                }
                this.setState({
                    alert_message: "success"
                });
            })
            .catch(error => {
                this.setState({
                    alert_message: "error"
                });
            });
    }

    onchangeCategoryName(e) {
        this.setState({
            category_name: e.target.value
        });
    }

    onSearch(e) {
        e.preventDefault();            
        if(this.state.category_name == null || this.state.category_name == ''){
            
        }else{
            axios
            .get(
                "http://127.0.0.1:8000/api/category/searchCategoryByName/" +
                    this.state.category_name
            )
            .then(response => {               
                this.setState({
                    categories: response.data
                });
            });
        }       
    }

    convertTime(time){
        return new Date(time.toda);
    }

    render() {
        return (
            <div>
                <hr />
                {this.state.alert_message == "success" ? (
                    <SuccessAlert message={"Category successfully deleted."} />
                ) : null}
                {this.state.alert_message == "error" ? <ErrorAlert /> : null}
                <form
                    onSubmit={this.onSearch}
                    className="form-inline my-2 my-lg-0"
                >
                    <input
                        className="form-control mr-sm-2"
                        type="search"
                        placeholder="Search"
                        aria-label="Search"
                        value={this.state.category_name}
                        onChange={this.onchangeCategoryName}
                    />
                    <button
                        className="btn btn btn-success mr-1 my-2 my-sm-0"
                        type="submit"
                    >
                        Search
                    </button>
                    <Link to="/category/Add">
                        <button
                            className="btn btn btn-primary my-2 my-sm-0"
                            type="submit"
                        >
                            Add
                        </button>
                    </Link>
                </form>
                &nbsp;
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Category Name</th>
                            <th scope="col">Status</th>
                            <th scope="col">Created At</th>
                            <th scope="col">Updated At</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.categories.map(category => {
                            return (
                                <tr key={category.id}>
                                    <th scope="row">{category.id}</th>
                                    <td>{category.name}</td>
                                    <td>
                                        {category.active == 1
                                            ? "Active"
                                            : "Inactive"}
                                    </td>
                                    <td>{category.created_at}</td>
                                    <td>{category.updated_at}</td>
                                    <td>
                                        <Link
                                            to={"category/edit/" + category.id}
                                        >
                                            Edit
                                        </Link>{" "}
                                        {" | "}
                                        <a
                                            href="#"
                                            onClick={this.onDelete.bind(
                                                this,
                                                category.id
                                            )}
                                        >
                                            Delete
                                        </a>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}
